# Circular Cylinder near a Plane Surface

M.TANAKA (tanaka19masahiro95@gmail.com)  
2023/01/21  

**I AM AFRAID THAT THIS PROJECT IS UNDER CONSTRUCTION. PLEASE COME BACK LATER.**



[TOC]

## 0. Lisence / ライセンス

This project is licensed under the MIT License, see the LICENSE file for details.  
本プロジェクトはMITライセンスとしている．詳しくはLICENSEファイルを参照されたい．

### 0.1 A little request for future users/modifiers / 利用者へのちょっとしたお願い

If you can, please show me the modified scripts for your aim. It'll be a valuable lesson for me.
For example, notifying me of your GitLab repository will be fully appreciated.  
本プロジェクトのコードを改変して利用された場合，可能であれば，どのように改変したのか作者に教えていただきたい．今後より良いコードを書く参考にしたい．例えば改変したコードのGitLabリポジトリを知らせてもらえたりすると，とても有難い．


## 1. 概要

CFDによるパラメータ最適化システムである．対象とする流れは単純であるが，システムに必要な以下の要素を備えている．このシステムを雛形として，より実際的な最適化システムを構築していくことができると考えている．

- PySideによるGUIウィザード
- optunaによる最適化システム
- FreeCADによるSTLファイル生成
- OpenFOAMによる流体解析
- ParaViewによる可視化

対象とする流れは，下記文献を参考にした．
- [檜和田宗彦・ほか3名, 平面壁近傍にある円柱まわりの流れ特性に及ぼす乱流境界層厚さの影響, 日本機械学会論文集, 52-479, B(1986), 2566.](http://japanlinkcenter.org/JST.Journalarchive/kikaib1979/52.2566)

この文献では，直径 $d$ の円柱の抗力係数 $C_D$ が平面壁との隙間 $s$ に応じて変化し， $s/d \approx 0.7$ で最大値をとることが示されている（ $\delta / d = 0.23$ のケース）．

最適化計算によってこのピークを正しく予測できるかを検証することで，本最適化システムの妥当性を確かめた．

|![](readme/cylinder_near_surface.png)|![](readme/cd_cl.png)|
|:-:|:-:|

※ 日本機械学会, 2013, 機械工学便覧DVD-ROM版 α4流体工学, p86

- 最適化における目的関数: 
    - 円柱の抗力係数 $C_D$ 
- 最適化における設計変数: 
    - 無次元隙間 $s / d$ 
    - 無次元角速度 $d \omega / U$ 

検証の結果，文献と同様に $s/d \approx 0.7$ で最大値をとることが確認できた．

|![](readme/optuna_result1.png)|![](readme/optuna_result2.png)|
|:-:|:-:|
| 試行回数に対する目的変数 $C_D$ の推移 | 設計変数 $s/d$ に対する目的変数 $C_D$ の関係 |

## 2. 使い方（利用者向け）

### 2.1 要件

- 対応OS
    - Windows (WSL使用．11でのみ検証)
    - Linux (Ubuntu22.04でのみ検証)

- 必要なソフトウェア
    - Miniconda
        - 環境構築については[別リポジトリ](https://gitlab.com/create-environment/miniconda)にまとめている．
            - [ ] submoduleでリポジトリに含める
        - 検証バージョンは23.3.1
    - OpenFOAM
        - インストール手順(Ubuntu)は[こちら](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled/debian#ubuntu)
        - 検証バージョンは2206


### 2.2 操作方法

- まず，本リポジトリのコードを画像のようにダウンロードし，任意のディレクトリに展開する．

    ![](readme/download_code.png)

- OSに応じて：
    - Windowsの場合，`EXECUTE-ME_windows.vbs`をダブルクリック．
    - Linuxの場合，`EXECUTE-ME_linux.sh`をコンソールで実行．

- 表示されたウィザードの`Help`ボタンをクリック．操作マニュアルがブラウザで開く．
    - 操作マニュアルの内容は[こちら](./src/help/help.md)からも確認できる．

- 開いたマニュアルの記述に従い操作する．

## 3. UML図（開発者向け）

### 3.1 ユースケース図

- [別ファイルに記述](./readme/uml1_usecase_diagram.md)

### 3.2 クラス図

- [別ファイルに記述](./readme/uml2_class_diagram.md)

### 3.3 シーケンス図

- [別ファイルに記述](./readme/uml3_sequence_diagram.md)


## 4. 作業メモ

### 4.0 全体

- 命名規則の統一
    - `dir_root` or `root_dir` ?
    - `folder_results` or `results_folder` ?

- htmlのkatex.min.cssの修正
```html
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@latest/dist/katex.min.css">
```

### 4.1 解析対象について

- 参考文献の研究では，円柱上流に配したトリッピングワイヤを使って乱流境界層厚さを変更している．層流境界層（トリッピングワイヤがない？）のとき$d = 15 \mathrm{mm}$に対し$\delta = 3.4 \mathrm{mm}, \therefore \delta / d = 0.23$となる．

    - ここで$\delta$は主流速度の99%に達する厚さと定義されている．
    - ??? どうやって$\delta$を計っている？熱線流速計？そのとき円柱は外している？

- このとき隙間に対する抗力係数のカーブは$C/d \sim 0.8$で顕著なピークを見せる．

- 非定常な現象なので，定常解析で求めても本質的にピークの一致は難しいかも

### 4.2 PySideによるGUIウィザード

- 何が起きているかよく知りたかったら：
    - 全ps1ファイルの`-WindowStyle hidden`オプションを削除．PowerShellプロンプトが表示され，標準出力が確認できる．
    - `vbs`ファイルではなく，`bat`ファイルを直接実行．同じくコマンドプロンプトが表示される．

- [x] シミュレーションの二重実行を防ぐメッセージ，ポップアップさせる．

- [x] Tab2DesignVariablesでスピンボックスに数字を入力しEnterすると，下のFreeCAD起動ボタンが実行される．思いがけずFreeCADが起動してしまうので困る．
    - [同様の質問あり](https://stackoverflow.com/questions/44005056/how-to-make-qpushbutton-not-to-be-triggered-by-enter-keyboard-key)
    - `setDefault(False)`，`setAutoDefault(False)`が重要？
    - できた．**全てのbuttonはこの設定を徹底する**．このプログラムでは：
        - `QPushButton`
        - `QCommandLinkButton`
        - `QDialogButtonBox.buttons()[i]`
    - ポップアップメッセージのボタンは，まあいいか．

- [x] optuna/main.pyの中からoptuna-dashboardを`subprocess.Popen`で非同期実行したが，mainがなぜか同期実行のため外に出てこれなかった模様．要修正．
    - [x] linuxは解消できた．`open_dashboard.sh`で`optuna-dashboard *`に` &`を追加するだけ．
    - [x] windowsは`open_dashboard.ps1`特にそのままでもいけた．

- [x] GUIウィザード上でfreecad起動すると，`subprocess.Popen`で開いてるのにウィザードは触れなくなる．非同期にしたい
    - [x] linuxは解消できた．`show_freecad.sh`で`freecad *.FCMacro`に` &`を追加するだけ．
    - [x] windowsは`show_freecad.ps1`特にそのままでもいけた．

- [ ] 最適化が完了したらはっきりわかるようにウィザードの表示を変えたい．
    - [ ] "Analyzing... Please do not close this wizard." → "Completed! You can close this wizard now."

- [ ] 最適化再開：今はdbファイルならなんでも受け付ける．今後カスタマイズが進めば，ウィザード毎に対象とする系は異なり，設計変数なども全く異なってくる．この不一致も弾けるようにしたい．
    - [ ] dbファイルの名前の先頭に，当該流れ特有の名前があるかで判定する．
    - [ ] 関連して，解析対象に対し一意に定まる名称の宣言箇所をコード内に明示する（今はマジックナンバーになっている）．

- [x] Inputクラスに従属して，ウィジェットの入力欄を生成するようにする（設計変数の数とか名前を読み取り反映する）．

- 設計変数は，固定なら最小最大値を同じ値にするだけでエラーなくOptunaが動く．

### 4.3 optunaによる最適化システム

- 多目的最適化は，１つの目的関数を除いて制約条件に置き換えることで単目的最適化に落とし込める．

- Leave One Out analysis?
- 

- 後から追加で最適化することができるようにする．

    - つまり実行前に「新規解析」「追加解析」を選ぶ機能が必要．
    - 過去の解析はどれが何か分からなくなることを考慮し，新規解析時にDescriptionを付けてもらう（jsonに書くか ~~study.system_attrsに書くとoptuna_dashboardで見れる！~~ 先のバージョンで廃止予定なのでやはりjsonに書く）．GUIで対応．
        - input_json に system_attrsに書き込む内容を指定．
        - Qtで書いた内容がjsonに出力される．サッと確認したいならメモ帳でjsonを開けばよい．

    - RDBファイルを指定の場所に保存できる必要があるが．．．やり方？
        - PowerShell/bashでカレントディレクトリを指定の場所に変更してからOptunaを実行すればよいことが分かった．

    - [ ] optunaの既存dbから追加解析可能にする．
        - [x] Windows
        - [ ] Linux
        - [x] Inputが上書きされてしまうので，numberOfTrialsのみ書き換わるようにする．

- [x] `results`フォルダの自動ネーミング
    - 最適化終了後，`results` → `results-2023-02-04-1242`という風に解析終了時刻で一意にリネームしたい．
    - 開始時にmkdirするようにした．

- [x] プロセスoptuna-dashboardが動き続けるので，KILLする機能を追加すべき
    - bashでは'pkill -f optuna-dashboard'で実現できる．

- [ ] optuna-dashboardにおいて"objective 0"を任意の名前に変更したい．

- [x] optunaの可視化ファイル出力のためにplotlyをインストール．
    - [minicondaインストーラ](https://gitlab.com/create-environment/miniconda)も修正

- [ ] バグ：Windowsでは，`subprocess.Popen`を使っても`Tab3Execution.run_optuna()`が同期実行になってしまう．（最適化が完了するまでGUIを触れないので，途中で強制終了できない）

- [ ] Force Stopがoptunaをkillするだけなので，実行中のOpenFOAMはendTimeまで走ったまま．
    - [ ] と思ったら次の試行も始まった．optunaがkillできていない？

- [ ] OpenFOAMのログファイルも記録として残すようにする．

- [ ] 設計変数は元は無次元数として与えるが，CADやCFDでは有次元数に換算される．現在はCADとCFDのそれぞれで独立に変換を行っている．食い違いの元になるので，Inputクラス内で変換処理を行ってしまいたい．
    - [ ] 設計変数にはそれぞれ取り得る値の範囲がある．その判定をInputクラスの中に実装したい．

- ターボ機械の解析の場合，設計点がサージングの範囲にないことを確認する過程を含める例があった．
    - 回転数同じで流量を変えて，圧力２点間の勾配が正か負かで判定

### 4.4 FreeCADによるSTLファイル生成

### 4.5 OpenFOAMによる流体解析

- フォルダ構成はOpenFOAMデフォルトのものから[少し改善を試みた](https://gitlab.com/openfoamscripts/tips/improve-readability-by-using-dictionaries/-/blob/main/slide.pdf)．

- [x] controlDictでendTimeを100に短縮している．後で直す．

- [ ] 解析が収束したかの判定に，ラスト100ステップの目的関数の標準偏差がある微少な値未満であること，という条件を加える．

- [ ] 最適化の結果得られたパラメータで，より詳細なメッシュで最終確認を行う機能が欲しい．

    - [ ] 関連して，spalart-allmarasモデルでターボ機械を解析した事例もある．１方程式で済むならその方が高速で良いのでは？

    - 最適化ではメッシュちょい粗目にして，最終結果だけしっかりやる例がある．応力解析もそう．

### 4.6 ParaViewによる可視化

### 4.7 Miniconda環境

- 環境がないときはPowerShellスクリプトにWrite-Hostするようにしているが，誤操作防止のためプロンプトは非表示にしたい．
- したがって，Write-Hostをポップアップメッセージに変えなければいけない．ps1だけでできる？

- 圧力pのカラーコンター図を出力する．


## 5. 参考：

- PlantUML
    - https://plantuml.com/ja/

- PySide
    - https://doc.qt.io/qtforpython/api.html
