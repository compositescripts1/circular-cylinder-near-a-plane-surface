# instance accessibility test

class Top():
    def __init__(self):
        self.a = A('alfa')
        self.b = B('bravo', self.a)
        return


class A():
    def __init__(self, name):
        self.name = name
        return
    
    def get_name(self):
        print(self.name)
        return self.name

    def set_name(self, newname):
        self.name = newname
        return


class B():
    def __init__(self, name, a):
        self.name = name
        self.a = a
        return
    
    def get_name(self):
        print(self.name)
        return self.name

    def set_name(self, newname):
        self.name = newname
        return

    def get_name_of_A(self):
        return self.a.get_name()



if __name__ == '__main__':
    t = Top()
    t.a.get_name()
    t.b.get_name()
    t.b.get_name_of_A()

    t.a.set_name('new_alfa')
    t.b.set_name('new_bravo')
    
    t.a.get_name()
    t.b.get_name()
    t.b.get_name_of_A()
    