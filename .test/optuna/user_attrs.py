import os
import sys
import logging
import optuna


optuna.logging.get_logger("optuna").addHandler(logging.StreamHandler(sys.stdout))
_name = 'cylinder-near-a-surface-2023-06-13T1032'
_storage: str = f"sqlite:///{_name}.db"
study: optuna.study.study.Study = optuna.create_study(study_name=_name, storage=_storage, load_if_exists=True) # Maximize the objective function.

attrs = study.user_attrs

print(attrs)
