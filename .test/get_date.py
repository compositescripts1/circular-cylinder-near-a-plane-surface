

import datetime


def get_results_folder_name():
    time0 = datetime.datetime.now()
    time1 = '{0:%Y-%m-%dT%H%M}'.format(time0) # ISO8601 anomaly
    return f'results-{time1}'

if __name__ == '__main__':
    folder_name = get_results_folder_name()
    print(folder_name)
