import json
import os

json_input = {
    'key1': 123,
    'key2': 'foobar'
}

pwd = os.path.dirname(__file__)

with open(f'{pwd}/output_test.json', 'w', encoding='utf-8') as file:
    json.dump(json_input, file, ensure_ascii=False, indent=4)
