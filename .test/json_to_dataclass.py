from typing import List, Tuple, Dict
import json
from dataclasses import dataclass
from dataclasses_json import LetterCase, dataclass_json


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Input:
    description: str
    objective_of_optimization: str
    parameters: Dict[str, List[float]]
    number_of_trials: int

class JsonToDataclass:
    def read_input_json(self):
        with open('input.json', 'r', encoding='utf-8') as file:
            input_json =  json.load(file)

        print(input_json)
        print(type(input_json))
        
        self.input: Input = Input.from_json(str(input_json).replace('\'', '"'))

        print(self.input)

        self.description = self.input.description
        self.objective_type = self.input.objective_of_optimization
        self.n = self.input.number_of_trials
        self.c = self.input.parameters['c: Clearance/Diameter']

        print(self.c)
        print(self.c[0])
        return

j = JsonToDataclass()
j.read_input_json()