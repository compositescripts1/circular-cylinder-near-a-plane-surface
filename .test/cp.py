import os
import subprocess
import sys

path: str = '/home/tanaka/_GitLab/compositeScripts/circular-cylinder-near-a-plane-surface/.test'

os.makedirs(f'{path}/test02/test01_copy', exist_ok=True)
p = subprocess.run("echo hello", shell=True, stdout=sys.stdout)
p = subprocess.run(f"cp {path}/test01/* {path}/test02/test01_copy", shell=True, stdout=sys.stdout)
