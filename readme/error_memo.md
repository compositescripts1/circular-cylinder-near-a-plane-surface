[TOC]

```
A new study created in RDB with name: cylinder-near-a-surface-2023-06-12T1741.db
C:\_GitLab\composite\circular-cylinder-near-a-plane-surface\results-2023-06-12T1741
cylinder-near-a-surface-2023-06-12T1741.db.db
```

## pwsh

- `Remove-Item`の仕様で，存在しないファイルを消そうとすると`ItemNotFoundException`をthrowしてしまう．エラーをスルーしたいが...

    - `-ErrorAction SilentlyContinue`を付け足すので良いか．

    ```powershell
    Remove-Item: C:\_GitLab\composite\circular-cylinder-near-a-plane-surface\src\simulation_template\step3_post-process\clean.ps1:4
    Line |
    4 |  Remove-Item png -Recurse
        |  ~~~~~~~~~~~~~~~~~~~~~~~~
        | Cannot find path
        | 'C:\_GitLab\composite\circular-cylinder-near-a-plane-surface\src\simulation_template\step3_post-process\png'
        | because it does not exist.
    ```

## Optuna

- (エラー)dbファイルを消して再度最適化を実行すると，時々以下のエラーが発生する．
    ```
    sqlite3.IntegrityError: UNIQUE constraint failed: alembic_version.version_num
    ```
    全文
    ```
    Traceback (most recent call last):
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\base.py", line 1900, in _execute_context
        self.dialect.do_execute(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\default.py", line 736, in do_execute
        cursor.execute(statement, parameters)
    sqlite3.IntegrityError: UNIQUE constraint failed: alembic_version.version_num

    The above exception was the direct cause of the following exception:

    Traceback (most recent call last):
    File "C:\Users\maoma\Documents\GitLab\composite\circular-cylinder-near-a-plane-surface\src\optuna\main.py", line 63, in <module>
        o = Optimize()
    File "C:\Users\maoma\Documents\GitLab\composite\circular-cylinder-near-a-plane-surface\src\optuna\main.py", line 15, in __init__
        self.study = optuna.create_study(study_name=title, storage=database_path)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\_convert_positional_args.py", line 63, in converter_wrapper
        return func(**kwargs)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\study\study.py", line 1156, in create_study
        storage = storages.get_storage(storage)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\__init__.py", line 43, in get_storage
        return _CachedStorage(RDBStorage(storage))
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\_rdb\storage.py", line 229, in __init__
        self._version_manager = _VersionManager(self.url, self.engine, self.scoped_session)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\_rdb\storage.py", line 1065, in __init__
        self._init_alembic()
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\_rdb\storage.py", line 1097, in _init_alembic
        self._set_alembic_revision(revision)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\_rdb\storage.py", line 1105, in _set_alembic_revision
        context.stamp(script, revision)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\alembic\runtime\migration.py", line 569, in stamp
        head_maintainer.update_to_step(step)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\alembic\runtime\migration.py", line 829, in update_to_step
        self._insert_version(vers)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\alembic\runtime\migration.py", line 765, in _insert_version
        self.context.impl._exec(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\alembic\ddl\impl.py", line 193, in _exec
        return conn.execute(  # type: ignore[call-overload]
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\base.py", line 1380, in execute
        return meth(self, multiparams, params, _EMPTY_EXECUTION_OPTS)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\sql\elements.py", line 334, in _execute_on_connection
        return connection._execute_clauseelement(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\base.py", line 1572, in _execute_clauseelement
        ret = self._execute_context(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\base.py", line 1943, in _execute_context
        self._handle_dbapi_exception(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\base.py", line 2124, in _handle_dbapi_exception
        util.raise_(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\util\compat.py", line 211, in raise_
        raise exception
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\base.py", line 1900, in _execute_context
        self.dialect.do_execute(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\sqlalchemy\engine\default.py", line 736, in do_execute
        cursor.execute(statement, parameters)
    sqlalchemy.exc.IntegrityError: (sqlite3.IntegrityError) UNIQUE constraint failed: alembic_version.version_num
    [SQL: INSERT INTO alembic_version (version_num) VALUES ('v3.0.0.d')]
    (Background on this error at: https://sqlalche.me/e/14/gkpj)
    ```

    - SQLiteのエラーであることしか分かっていない．
    - dbファイルを削除した後ゴミ箱も空にすると大丈夫っぽい？
    - `Remove-Item`で完全削除するのがベター

    - [ ] 上記エラーはどうやら，Qtインスタンスを起動したまま2度目のoptuna最適化を実行しても生じるらしい．
        - なので，`Run`ボタンは一回しか押せないようにして，２回目以降はメッセージボックスで再起動を促す．
        - 対症療法感が強いので，再起動不要になるまでチェックボックス空けておく．

    - そもそもdbファイルの名前が一意でないことが原因？？
        - 時刻で名前を一意に定めるように変更してみる
            - エラーは起こった．
    
    - どうやら，二度目の解析時にはdbファイルは作成されるが内部にstudy_nameを登録する段階でエラーが生じているらしい．

    ```python
    Python 3.10.8 | packaged by conda-forge | (main, Nov 22 2022, 08:16:33) [MSC v.1929 64 bit (AMD64)] on win32
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import optuna                                                                                                                
    >>> study = optuna.load_study('cylinder-near-a-surface-2023-02-05T1047', 'sqlite:///cylinder-near-a-surface-2023-02-05T1047.db')
    <stdin>:1: FutureWarning: load_study(): Please give all values as keyword arguments. See https://github.com/optuna/optuna/issues/3324 for details.
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\_convert_positional_args.py", line 63, in converter_wrapper
        return func(**kwargs)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\study\study.py", line 1260, in load_study
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\_cached_storage.py", line 136, in get_study_id_from_name
        return self._backend.get_study_id_from_name(study_name)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\_rdb\storage.py", line 335, in get_study_id_from_name
        study = models.StudyModel.find_or_raise_by_name(study_name, session)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\storages\_rdb\models.py", line 82, in find_or_raise_by_name
        raise KeyError(NOT_FOUND_MSG)
    KeyError: 'Record does not exist.'
    ```
    
    - PowerShellでも，

    ```powershell
    (opencae) PS > optuna studies --storage sqlite:///cylinder-near-a-surface-2023-02-05T1047.db
    Traceback (most recent call last):
    File "C:\Users\maoma\miniconda3\envs\opencae\Scripts\optuna-script.py", line 9, in <module>
        sys.exit(main())
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\cli.py", line 1052, in main
        return args.handler(args)
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\cli.py", line 411, in take_action
        _format_output(
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\cli.py", line 212, in _format_output
        return _dump_table(values, header).strip()
    File "C:\Users\maoma\miniconda3\envs\opencae\lib\site-packages\optuna\cli.py", line 181, in _dump_table
        max_width = max(len(header[column]), max(row[column].width() for row in rows))
    ValueError: max() arg is an empty sequence
    (opencae) PS > cd ..\results-2023-02-05T1042\
    (opencae) PS > optuna studies --storage sqlite:///cylinder-near-a-surface-2023-02-05T1042.db
    +-----------------------------------------+---------------+----------+---------------------+
    | name                                    | direction     | n_trials | datetime_start      |
    +-----------------------------------------+---------------+----------+---------------------+
    | cylinder-near-a-surface-2023-02-05T1042 | ('MINIMIZE',) |       20 | 2023-02-05 10:42:29 |
    +-----------------------------------------+---------------+----------+---------------------+
    ```
    
    - そもそも`optuna.study.Study.system_attrs`に変更を加えるのがあかんっぽいので当該操作自体を削除．
        - エラーは消えた．
        - dbファイル内にメモを書き込みたい場合，optuna-dashboardで開いてから，ブラウザ上で書き込むのが素直だろう
        - 上記メモは多分`user_attrs`に書き込まれるのかな？後で確認してみたい．

## OpenFOAM

- OpenMPI関連

    - 今使っているWindowsではsimpleFoamのログに以下の警告がでる．

    ```
    --------------------------------------------------------------------------
    WARNING: Linux kernel CMA support was requested via the
    btl_vader_single_copy_mechanism MCA variable, but CMA support is
    not available due to restrictive ptrace settings.

    The vader shared memory BTL will fall back on another single-copy
    mechanism if one is available. This may result in lower performance.

    Local host: ***
    --------------------------------------------------------------------------
    
    --------------------------------------------------------------------------
    A system call failed during shared memory initialization that should
    not have.  It is likely that your MPI job will now either abort or
    experience performance degradation.

    Local host:  ***
    System call: munmap(2) 
    Error:       Invalid argument (errno 22)
    --------------------------------------------------------------------------
    ```