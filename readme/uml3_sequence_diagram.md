# シーケンス図
M.TANAKA  
作成：2023/05/06  
編集：2023/06/20  

[READMEへ](../README.md#33-シーケンス図)
[クラス図へ](./uml2_class_diagram.md)

[TOC]

## GUI除く

```plantuml 
participant Shell [
    PowerShell / Bash
]
participant Shell2 [
    PowerShell / Bash
]
participant Shell3 [
    PowerShell / Bash
]
participant Optimize [
    src/optuna/main.py
    ----
    class Optimize
]
participant Json [
    src/results-*/input.json
]

activate Shell
ref over Shell
  This is main thread.
end ref

Shell    -> Optimize: run_optuna.ps1/sh
activate Optimize

Optimize ->   Json:     read_input_json()
Optimize <<-- Json:     input: Input

Shell2   <-   Optimize:    open_dashboard.ps1/sh
activate Shell2
note left
  Optuna-dashboard 
  will opened by browser
end note

Shell3   <-   Optimize: run().objective()
activate Shell3
Shell3   ->   Shell3:   simulation_template/Allrun.sh
note left
  Run OpenFOAMs
end note

Shell3   -->> Optimize: objective value
destroy Shell3

Shell    <<-- Optimize
destroy Optimize

ref over Shell2
  This thread remains.
end ref
```

### MEMO

- [x] プロセスoptuna-dashboardが動き続けるので、KILLする機能を追加すべき
  - GUIウィザードを閉じると同時に`src/optuna/close_dashboard.sh`が実行されるようにした．

## GUI含む

