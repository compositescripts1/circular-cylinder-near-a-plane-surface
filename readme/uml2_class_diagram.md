# クラス
M.TANAKA  
作成：2023/06/20  
編集：2023/06/20  

- [READMEへ](../README.md#32-クラス図)
- [ユースケース図へ](./uml1_usecase_diagram.md)

---

[TOC]

## クラス図

```plantuml
package "package 最適化GUI" 
{
    class MainFrame 
    {
    }
    class Tab1Objective 
    {
    }
    class Tab2DesignVariables 
    {
    }
    class Tab3Execution 
    {
    }

    MainFrame *-- Tab1Objective
    MainFrame *-- Tab2DesignVariables
    MainFrame *-- Tab3Execution
}

class Optimize 
{
}

class Input 
{
    design_variables: dict[str, list[float]]
    number_of_trials: int
}

class DesignVariableNames 
{
    names: list[str]
}

Input o-- DesignVariableNames

class SimInput 
{
    design_variables: dict[str, float]
    number_of_trial: int
}

class SimOutput 
{
    objective_variable: float
    number_of_trial: int
}

SimInput .. SimOutput
```


---

- [シーケンス図へ](./uml3_sequence_diagram.md)