#!/bin/bash
cd "${0%/*}" || exit

datetime_now=$1

dir_src=$(pwd)                 # /src
dir_root=$(dirname "$dir_src") # /


# Source shell scripts to activate miniconda
. "$dir_src"/miniconda/initialize_miniconda.sh
. "$dir_src"/miniconda/activate_opencae.sh

# Run optimization
cd "$dir_root"/results-"$datetime_now" || exit # optuna's RDB file will be created here. 
python "$dir_src"/optuna/main.py new "$datetime_now"

# Return to the directory where we were
cd "$dir_src" || exit

