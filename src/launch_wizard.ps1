$dir_src = $PSScriptRoot # /src

# Source PowerShell scripts to activate miniconda
. $dir_src/miniconda/initialize_miniconda.ps1
. $dir_src/miniconda/activate_gui.ps1

# launch the wizard
python $dir_src/pyside/main.py
