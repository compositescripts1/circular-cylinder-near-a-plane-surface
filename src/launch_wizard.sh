#!/bin/bash
cd "${0%/*}" || exit
dir_src=$(pwd)

# Source shell scripts to activate miniconda
. "$dir_src"/miniconda/initialize_miniconda.sh
. "$dir_src"/miniconda/activate_gui.sh

# launch the wizard
python "$dir_src"/pyside/main.py
