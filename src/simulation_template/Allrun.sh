#!/bin/bash
cd "${0%/*}" || exit                                # Run from this directory
#------------------------------------------------------------------------------

dir_sim=$(pwd) # /src/simulation_template/

echo "step1..."
cd step1_pre-process || exit
./run.sh 

echo "step2..."
cd ../step2_solve || exit
./run.sh 

echo "step3..."
cd ../step3_post-process || exit
./run.sh 


cd "$dir_sim" || exit
