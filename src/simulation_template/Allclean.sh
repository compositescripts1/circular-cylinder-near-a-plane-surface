#!/bin/bash
cd "${0%/*}" || exit                                # Run from this directory
#------------------------------------------------------------------------------

dir_sim=$(pwd) # /src/simulation_template/

rm sim_output.json

cd step1_pre-process || exit
./clean.sh 

cd ../step2_solve || exit
./clean.sh 

cd ../step3_post-process || exit
./clean.sh 


cd "$dir_sim" || exit
