#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
#------------------------------------------------------------------------------

dir_step2=$(pwd)                # /src/simulation_template/step2_solve
dir_tmp=$(dirname "$dir_step2") # /src/simulation_template
dir_src=$(dirname "$dir_tmp")   # /src

# Source shell scripts to activate miniconda
. "$dir_src"/miniconda/initialize_miniconda.sh
. "$dir_src"/miniconda/activate_opencae.sh

# set design variables
python ./set_variables.py

# solve by OpenFOAM
./solve.sh

cd "$dir_step2" || exit