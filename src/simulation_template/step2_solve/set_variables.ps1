param(
    [Parameter()]
    [String]$var1,
    [String]$var2
)

# set variables by using OpenFOAM function
wsl -e ./set_variables.sh "$var1" "$var2"
