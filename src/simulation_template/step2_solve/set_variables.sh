#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
. ~/.bashrc                                         # to run correctly via wsl.exe
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------
# about foamDictionary, see:
#   https://www.cfdengine.com/newsletter/082/

nondim_s=$1
nondim_omega=$2

foamDictionary myDictionaries/boundaryConditions \
    -entry boundaryConditions.noSlipWall.U.rotating._s \
    -set "$nondim_s" \
    -disableFunctionEntries

foamDictionary myDictionaries/boundaryConditions \
    -entry boundaryConditions.noSlipWall.U.rotating._omega \
    -set "$nondim_omega" \
    -disableFunctionEntries
    