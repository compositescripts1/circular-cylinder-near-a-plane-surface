import os
import subprocess
import sys

dir_st2 = os.path.dirname(__file__)
dir_sim = os.path.dirname(dir_st2)
sys.path.append(dir_sim)
from sim_input  import *

dir_src = os.path.dirname(dir_sim)
dir_opt = f'{dir_src}/optuna'
sys.path.append(dir_opt)
from input import *


class SetVariables:
    """Set design variables
    set design variables of single simulation.
    """
    def __init__(self) -> None:
        self.read_inputs()

        print('__init__ finished.')
        return


    def read_inputs(self) -> None:
        # read Input
        d = DesignVariableNames()
        self.names: list[str] = d.names

        # read SimInput
        with open(f'{dir_sim}/sim_input.json', 'r', encoding='utf-8') as file:
            sim_input_json: dict =  json.load(file)
                
        sim_input: SimInput = SimInput.from_json(str(sim_input_json).replace('\'', '"'))
        self.variables : dict[str, float] = sim_input.design_variables
        return
    

    def main(self) -> None:
        var1 = str(self.variables[self.names[0]])
        var2 = str(self.variables[self.names[1]])

        if os.name == 'nt':
            set_variables_ps1: str = f'{dir_st2}/set_variables.ps1'
            p = subprocess.Popen(["powershell.exe", set_variables_ps1, var1, var2], stdout=sys.stdout)
            p.communicate()
        elif os.name == 'posix':
            set_variables_sh: str = f'{dir_st2}/set_variables.sh'
            p = subprocess.Popen(["bash", set_variables_sh, var1, var2], stdout=sys.stdout)
            p.communicate()
        return


if __name__ == '__main__':
    s = SetVariables()
    s.main()
