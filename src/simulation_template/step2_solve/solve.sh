#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
. ~/.bashrc                                         # to run correctly via wsl.exe
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------

dir_st2=$(pwd) # /src/simulation_template/step2_solve

decompDict="-decomposeParDict system/dicts/parallelization/decomposeParDict"

touch para.foam

echo "copy stl..."

mkdir -p constant/triSurface

cp -f \
    ../step1_pre-process/stl/geometry.stl \
    constant/triSurface/

cd constant/triSurface || exit
surfaceConvert -scale 0.001 geometry.stl geometry_SI.stl
surfaceFeatureEdges geometry_SI.stl geometry_SI.fms -angle 0
mv ./geometry_SI.fms ../../geometry_SI.fms

echo "create mesh..."

cd "$dir_st2" || exit
runApplication cartesian2DMesh
runApplication renumberMesh -overwrite
runApplication checkMesh -writeFields '(nonOrthoAngle)' -constant

echo "solve..."

restore0Dir
cp system/fvSchemes.stable system/fvSchemes
runApplication  potentialFoam -writep

runApplication $decompDict decomposePar
runParallel $decompDict $(getApplication)
runApplication reconstructParMesh -constant
runApplication reconstructPar -latestTime
