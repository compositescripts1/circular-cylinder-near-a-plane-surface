$dir_step2 = $PSScriptRoot  # /src/simulation_template/step2_solve
Set-Location $dir_step2

# Source PowerShell scripts to activate miniconda
$dir_src = Split-Path(Split-Path $dir_step2) # /src/
. $dir_src/miniconda/initialize_miniconda.ps1
. $dir_src/miniconda/activate_opencae.ps1

# set design variables
python ./set_variables.py

# solve by OpenFOAM
wsl -e ./solve.sh

Set-Location $dir_step2
