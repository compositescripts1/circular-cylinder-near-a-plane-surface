#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
. ~/.bashrc                                         # to run correctly via wsl.exe
. ${WM_PROJECT_DIR:?}/bin/tools/CleanFunctions      # Tutorial clean functions
#------------------------------------------------------------------------------

# cleanCase0
cleanCase

# Remove surface and features
rm -r 0
rm -r constant/triSurface
rm constant/nonOrthoAngle
rm *.fms


#------------------------------------------------------------------------------
