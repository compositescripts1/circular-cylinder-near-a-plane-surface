/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2206                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/

fluidProperties
{
    air
    {
        incompressible
        {
            // incompressible (20 degree Celsius)
            rho             1.204; // kg/m^3
            mu              1.825e-05;  // Pa*s
            nu              #eval "$mu / $rho"; // m^2/s
        }

        specie
        {
            nMoles          1;
            molWeight       28.9;
        }

        thermodynamics
        {
            hConst
            {
                Cp          1007;
                Hf          0;
            }
        }

        transport
        {
            const
            {
                mu          1.82e-05;
                Pr          0.71;
            }
            sutherland
            {
                mu0         1.716e-05;  // Pa*s
                T0          273.11;     // K
                Ts          110.56;     // K
                As          #eval "$mu0 * ($T0 + $Ts) / pow($T0, 1.5)"; // Pa*s*K^(-0.5)
            }
        }
    }

    water
    { 
        // heat transfar (20 degree Celsius)
        specie
        {
            nMoles      1;
            molWeight   18.01528;
        }
        hConst
        {
            Cp          4181.8;
            Hf          -2.45e6;    // J/Kg
        }
        const
        {
            mu          1.002e-03;  // Pa*s
            Pr          6.99;
        }

        // incompressible (20 degree Celsius)
        transportModel  Newtonian;
        rho             998.21;
        nu              #eval "$const.mu / $rho"; // m^2/s
    }

    reference
    {
        ref1    "https://www.engineersedge.com/physics/viscosity_of_air_dynamic_and_kinematic_14483.htm";
        ref2    "https://doc.comsol.com/5.5/doc/com.comsol.help.cfd/cfd_ug_fluidflow_high_mach.08.27.html";
        ref3    "https://www.afs.enea.it/project/neptunius/docs/fluent/html/ug/node294.htm";
    }
}

// ************************************************************************* //
