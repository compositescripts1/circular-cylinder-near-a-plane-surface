$dir_step3 = $PSScriptRoot  # /src/simulation_template/step3_post-process
Set-Location $dir_step3

# Source PowerShell scripts to activate miniconda
$dir_src = Split-Path(Split-Path $dir_step3) # /src/
. $dir_src/miniconda/initialize_miniconda.ps1
. $dir_src/miniconda/activate_opencae.ps1

python post.py
