from io import StringIO
import os
import re
import sys
import matplotlib.pyplot as plt
import pandas as pd

dir_st3 = os.path.dirname(__file__)
dir_sim = os.path.dirname(dir_st3)
sys.path.append(dir_sim)
from sim_input import *
from sim_output import *


class Plot2DGraph():
    end_time: int = 1000 # must be same with EndTime in controlDict

    cd       : pd.core.frame.DataFrame
    cl       : pd.core.frame.DataFrame
    residuals: pd.core.frame.DataFrame

    def __init__(self) -> None:
        self.dir_st3 : str = os.path.dirname(__file__)     # /src/simulation_template/step3_post-processing/
        self.dir_sim : str = os.path.dirname(self.dir_st3) # /src/simulation_template/
        self.dir_src : str = os.path.dirname(self.dir_sim) # /src/
        self.dir_root: str = os.path.dirname(self.dir_src) # /
        self.dir_src  = self.dir_src.replace('\\', '/')
        self.dir_root = self.dir_root.replace('\\', '/')

        os.makedirs(f'{self.dir_st3}/png', exist_ok=True)
        return
    

    def read_sim_input_json(self) -> None:
        with open(f'{self.dir_sim}/sim_input.json', 'r', encoding='utf-8') as file:
            sim_input_json: dict =  json.load(file)
                
        sim_input: SimInput = SimInput.from_json(str(sim_input_json).replace('\'', '"'))
        self.objective_of_optimization : str = sim_input.objective_of_optimization
        self.number: int = sim_input.number_of_trial
        return
    

    def get_residuals(self) -> None:
        with open(f'{self.dir_sim}/step2_solve/postProcessing/solverInfo/0/solverInfo.dat', 'r') as file:
            planetxt: str = file.read()

        planetxt = re.sub('^# ', '', planetxt, flags=re.MULTILINE)

        df: pd.core.frame.DataFrame = pd.read_csv(StringIO(planetxt), delim_whitespace=True, skiprows=1)
        self.residuals = df.loc[:, ['p_initial', 'Ux_initial', 'Uy_initial', 'k_initial', 'omega_initial']]   
        return


    def get_force_coefficients(self) -> None:
        with open(f'{self.dir_sim}/step2_solve/postProcessing/forceCoeffs/0/coefficient.dat', 'r') as file:
            planetxt: str = file.read()

        planetxt = re.sub('^# ', '', planetxt, flags=re.MULTILINE)

        df: pd.core.frame.DataFrame = pd.read_csv(StringIO(planetxt), delim_whitespace=True, skiprows=12)
        self.cd = df.loc[:, ['Cd']]
        self.cl = df.loc[:, ['Cl']]
        return


    def plot_residuals(self) -> None:
        self.residuals.plot()
        plt.xlabel('iteration')
        plt.ylabel('residuals [-]')
        plt.semilogy()
        plt.xlim(0, self.end_time)
        plt.ylim(1e-7, 1)
        plt.grid()
        plt.savefig(f'{self.dir_st3}/png/residuals.png')
        plt.close()
        return


    def plot_force_coefficients(self) -> None:
        self.cd.plot()
        plt.xlabel('iteration')
        plt.ylabel('drag coefficient [-]')
        plt.xlim(0, self.end_time)
        plt.ylim(0, 1.5)
        plt.grid()
        plt.savefig(f'{self.dir_st3}/png/drag_coefficient.png')
        plt.close()

        self.cl.plot()
        plt.xlabel('iteration')
        plt.ylabel('lift coefficient [-]')
        plt.xlim(0, self.end_time)
        plt.ylim(-0.1, 0.5)
        plt.grid()
        plt.savefig(f'{self.dir_st3}/png/lift_coefficient.png')
        plt.close()
        return


    def write_sim_output_json(self) -> None:
        forcecoeff     : pd.core.frame.DataFrame

        if self.objective_of_optimization == 'Maximize drag coefficient':
            forcecoeff = self.cd
        elif self.objective_of_optimization == 'Maximize lift coefficient':
            forcecoeff = self.cl
        else:
            pass

        last_rows      : pd.core.frame.DataFrame = forcecoeff.tail(int(self.end_time/10))
        last_rows_mean : pd.core.series.Series   = last_rows.mean()
        forcecoeff_mean: float                   = last_rows_mean.loc['Cd']

        sim_output = SimOutput(
            objective_variable = forcecoeff_mean,
            number_of_trial = self.number
        )

        with open(f'{self.dir_sim}/sim_output.json', 'w', encoding='utf-8') as file:
            file.write(sim_output.to_json(ensure_ascii=False, indent=4))
        return


if __name__ == '__main__':
    p = Plot2DGraph()
    p.read_sim_input_json()
    p.get_residuals()
    p.get_force_coefficients()
    p.plot_residuals()
    p.plot_force_coefficients()
    p.write_sim_output_json()
