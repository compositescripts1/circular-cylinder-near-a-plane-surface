#!/bin/bash
cd "${0%/*}" || exit

dir_st3=$(pwd)                # /src/simulation_template/step3_post-process
dir_tmp=$(dirname "$dir_st3") # /src/simulation_template
dir_src=$(dirname "$dir_tmp") # /src

# Source shell scripts to activate miniconda
. "$dir_src"/miniconda/initialize_miniconda.sh
. "$dir_src"/miniconda/activate_opencae.sh

python post.py
