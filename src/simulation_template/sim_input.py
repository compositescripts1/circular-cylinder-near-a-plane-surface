import json
from dataclasses import dataclass
from dataclasses_json import LetterCase, dataclass_json


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class SimInput:
    """Input data class for simulation by OpenFOAM.
            
        the json file looks like:
        {
            "objectiveOfOptimization": "Maximize drag coefficient",
            "designVariables": {
                "c: Clearance/Diameter": 0.1,
                ...
                "xxx"                  : 9.9
            },
            "numberOfTrial": 1 // in this case, it's the first simulation 
        }
    """
    objective_of_optimization: str
    design_variables: dict[str, float]
    # There is 1 key: 'c: Clearance/Diameter'
    number_of_trial: int
