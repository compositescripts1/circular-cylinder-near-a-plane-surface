$dir_sim = $PSScriptRoot        # /src/simulation_template/
Set-Location $dir_sim || exit   # Run from this directory
#------------------------------------------------------------------------------

Set-Location step1_pre-process || exit
./run.ps1 

Set-Location ../step2_solve || exit
./run.ps1 

Set-Location ../step3_post-process || exit
./run.ps1 


Set-Location $dir_sim || exit
