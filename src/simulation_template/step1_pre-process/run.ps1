$dir_step1 = $PSScriptRoot                   # /src/simulation_template/step1_pre-process
Set-Location $dir_step1

# Source PowerShell scripts to activate miniconda
$dir_src = Split-Path(Split-Path $dir_step1) # /src/
. $dir_src/miniconda/initialize_miniconda.ps1
. $dir_src/miniconda/activate_opencae.ps1

freecadcmd fc_createstl.py

Get-ChildItem -File "$dir_step1/stl" | Rename-Item -NewName { $_.Name -Replace "\.ast", ".stl" }
Get-ChildItem -File "$dir_step1/stl" |
ForEach-Object {
    $name = [System.IO.Path]::GetFileNameWithoutExtension($_.Name)
    $reader = New-Object IO.StreamReader($_.FullName, [System.Text.Encoding]::GetEncoding("utf-8"))
    $content = ""
    while (!$reader.EndOfStream) {
        $str = $reader.ReadLine() -creplace "solid", "solid $name"
        $content += "$str`r`n"
    }
    $reader.Close()
    $writer = New-Object IO.StreamWriter($_.FullName, $false, [System.Text.Encoding]::GetEncoding("utf-8")) # $true: append | $false: overwrite
    $writer.Write($content)
    $writer.Close()
}
Get-Content "$dir_step1/stl/inlet.stl", "$dir_step1/stl/outlet.stl", "$dir_step1/stl/cylinder.stl", "$dir_step1/stl/noslip.stl", "$dir_step1/stl/slip.stl" | Out-File "$dir_step1/stl/geometry.stl"

Set-Location $dir_step1