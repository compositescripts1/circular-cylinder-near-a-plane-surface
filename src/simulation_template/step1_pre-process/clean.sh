#!/bin/bash
cd "${0%/*}" || exit

dir_step1=$(pwd)                # /src/simulation_template/step1_pre-process

rm "$dir_step1"/*.FCStd
rm "$dir_step1"/*.FCStd1
rm -r "$dir_step1"/stl
