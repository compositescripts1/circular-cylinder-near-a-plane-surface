$dir_pwd = Get-Location
$dir_st1 = $PSScriptRoot       # /src/simulation_template/step1_pre-process
$dir_tmp = Split-Path $dir_st1 # /src/simulation_template
$dir_src = Split-Path $dir_tmp # /src/

# Source PowerShell scripts to activate miniconda
. $dir_src/miniconda/initialize_miniconda.ps1
. $dir_src/miniconda/activate_opencae.ps1

# Open FreeCAD file
Set-Location $dir_st1
freecad ./show_geometry.FCMacro

Set-Location $dir_pwd
