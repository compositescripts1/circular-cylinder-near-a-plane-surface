$dir_step1 = $PSScriptRoot                   # /src/simulation_template/step1_pre-process
Set-Location $dir_step1

Remove-Item -Path "$dir_step1/*.FCStd" -ErrorAction SilentlyContinue
Remove-Item -Path "$dir_step1/*.FCStd1" -ErrorAction SilentlyContinue
Remove-Item -Path "$dir_step1/stl" -Recurse -ErrorAction SilentlyContinue
