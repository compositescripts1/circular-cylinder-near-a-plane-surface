#!/bin/bash
cd "${0%/*}" || exit

dir_st1=$(pwd)                # /src/simulation_template/step1_pre-process
dir_tmp=$(dirname "$dir_st1") # /src/simulation_template
dir_src=$(dirname "$dir_tmp") # /src

# Source PowerShell scripts to activate miniconda
. "$dir_src"/miniconda/initialize_miniconda.sh
. "$dir_src"/miniconda/activate_opencae.sh

# Open FreeCAD file
cd "$dir_st1" || exit
freecad ./show_geometry.FCMacro & # wanna be independent with pyside wizard

cd "$dir_st1" || exit
