# FreeCAD 0.20.2
# Python 3.10

import os
from math import pi
import sys

import FreeCAD as App
import Part
import Sketcher

dir_st1 = os.path.dirname(__file__)
dir_sim = os.path.dirname(dir_st1)
sys.path.append(dir_sim)
from sim_input  import *

dir_src = os.path.dirname(dir_sim)
dir_opt = f'{dir_src}/optuna'
sys.path.append(dir_opt)
from input import *


class CreateStl:
    dir_st1: str = os.path.dirname(__file__)   # /src/simulation_template/step1_pre-process
    dir_sim: str = os.path.dirname(dir_st1)    # /src/simulation_template/

    START : int = 1
    END   : int = 2
    CENTER: int = 3
    X_AXIS: int = -1
    Y_AXIS: int = -2

    x_max  : float = 1000.0
    x_min  : float = -1000.0
    x_le   : float = -625.0 # leading edge

    y_max  : float = 250.0
    y_min  : float = -50.0
    y_plate: float = -10.0

    radius : float = 7.5

    doc:      App.Document
    body_0:   Part.BodyBase
    body_1:   Part.BodyBase
    sketch_0: Sketcher.Sketch
    sketch_1: Sketcher.Sketch
    sketch_2: Sketcher.Sketch
    sketch_3: Sketcher.Sketch
    pad:      Part.Feature
    pocket:   Part.Feature
    region_0: Part.Feature
    region_1: Part.Feature
    # sheet:    What.Class.Is.It (there's no Spreadsheet.Sheet)


    def __init__(self) -> None:
        self.design_variable_names = DesignVariableNames()
        self.read_sim_input_json()

        self.doc      = App.newDocument('cylinder_near_a_wall')
        self.body_0   = self.doc.addObject('PartDesign::Body', 'body_0')
        self.body_1   = self.doc.addObject('PartDesign::Body', 'body_1')
        self.sketch_0 = self.body_0.newObject('Sketcher::SketchObject', 'sketch_0')
        self.sketch_1 = self.body_0.newObject('Sketcher::SketchObject', 'sketch_1')
        self.sketch_2 = self.body_1.newObject('Sketcher::SketchObject', 'sketch_2')
        self.sketch_3 = self.body_1.newObject('Sketcher::SketchObject', 'sketch_3')
        self.sheet    = self.doc.addObject('Spreadsheet::Sheet', 'sheet')
        self.doc.recompute()
        print('__init__ finished.')
        return


    def read_sim_input_json(self) -> None:
        with open(f'{self.dir_sim}/sim_input.json', 'r', encoding='utf-8') as file:
            sim_input_json: dict =  json.load(file)
                
        sim_input: SimInput = SimInput.from_json(str(sim_input_json).replace('\'', '"'))
        design_variables : dict[str, float] = sim_input.design_variables
        self.var0: float = design_variables[self.design_variable_names.names[0]]
        return
    

    def set_sheet(self) -> None:
        self.sheet.set('A1', 'Parameter')
        self.sheet.set('B1', 'Value')
        self.sheet.set('C1', 'Unit')
        self.sheet.set('D1', 'IsIndependent')

        self.write_parameter(sheet=self.sheet, row='2', name='C_per_d', expression=str(self.var0), unit='-', is_indepemdent=True)

        expression: str = f'= C_per_d * {str(2*self.radius)} + {str(self.radius)}'
        self.write_parameter(sheet=self.sheet, row='3', name='wall_to_center', expression=expression, unit='mm', is_indepemdent=False)

        self.doc.recompute()
        print('set_sheet finished.')
        return
    

    def write_parameter(self, sheet, row:str, name:str, expression:str, unit:str, is_indepemdent:bool) -> None:
        BLUE: tuple[float, float, float] = (0.5, 1.0, 1.0)
        GRAY: tuple[float, float, float] = (0.8, 0.8, 0.8)

        sheet.set('A' + row, name)
        sheet.set('B' + row, expression)
        sheet.setAlias('B' + row, name)
        sheet.set('C' + row, unit)
        if is_indepemdent:
            sheet.set('D' + row, 'True')
            sheet.setBackground('D' + row, BLUE)
        else:
            sheet.set('D' + row, 'False')
            sheet.setBackground('D' + row, GRAY)
        return

    
    def set_sketch_0(self) -> None:
        self.sketch_0.Support = (self.doc.getObject('XY_Plane'), [''])
        self.sketch_0.MapMode = 'FlatFace'
        self.doc.recompute()

        points: list[App.Vector] = [
            App.Vector(self.x_le,  0.0,          0.0),
            App.Vector(self.x_max, 0.0,          0.0),
            App.Vector(self.x_max, self.y_max,   0.0),
            App.Vector(self.x_min, self.y_max,   0.0),
            App.Vector(self.x_min, self.y_min,   0.0),
            App.Vector(self.x_max, self.y_min,   0.0),
            App.Vector(self.x_max, self.y_plate, 0.0),
            App.Vector(self.x_le,  self.y_plate, 0.0)
        ]

        lines: list[Part.LineSegment] = [
            Part.LineSegment(points[0], points[1]),
            Part.LineSegment(points[1], points[2]),
            Part.LineSegment(points[2], points[3]),
            Part.LineSegment(points[3], points[4]),
            Part.LineSegment(points[4], points[5]),
            Part.LineSegment(points[5], points[6]),
            Part.LineSegment(points[6], points[7]),
            Part.LineSegment(points[7], points[0])
        ]

        geo_lines: tuple[int, ...] = self.sketch_0.addGeometry(lines, False)
        self.doc.recompute()

        nonparametric_constraints_input: list[Sketcher.Constraint] = [
            Sketcher.Constraint('PointOnObject', geo_lines[0], self.START, self.X_AXIS),
            Sketcher.Constraint('PointOnObject', geo_lines[0], self.END,   self.X_AXIS),
            Sketcher.Constraint('Vertical',   geo_lines[1]),
            Sketcher.Constraint('Horizontal', geo_lines[2]),
            Sketcher.Constraint('Vertical',   geo_lines[3]),
            Sketcher.Constraint('Horizontal', geo_lines[4]),
            Sketcher.Constraint('Vertical',   geo_lines[5]),
            Sketcher.Constraint('Horizontal', geo_lines[6]),
            Sketcher.Constraint('Coincident', geo_lines[1], self.START, geo_lines[0], self.END),
            Sketcher.Constraint('Coincident', geo_lines[2], self.START, geo_lines[1], self.END),
            Sketcher.Constraint('Coincident', geo_lines[3], self.START, geo_lines[2], self.END),
            Sketcher.Constraint('Coincident', geo_lines[4], self.START, geo_lines[3], self.END),
            Sketcher.Constraint('Coincident', geo_lines[5], self.START, geo_lines[4], self.END),
            Sketcher.Constraint('Coincident', geo_lines[6], self.START, geo_lines[5], self.END),
            Sketcher.Constraint('Coincident', geo_lines[7], self.START, geo_lines[6], self.END),
            Sketcher.Constraint('Coincident', geo_lines[0], self.START, geo_lines[7], self.END)
        ]
        con_nonparam: tuple[int, ...] = self.sketch_0.addConstraint(nonparametric_constraints_input)
        self.doc.recompute()

        parametric_constraints_input: list[Sketcher.Constraint] = [
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[0], self.START, abs(self.x_le)),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[0], self.END, self.x_max),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[2], self.y_max),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[3], self.x_max),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[4], abs(self.y_min)),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[5], self.x_max),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[6], abs(self.y_plate)),
            Sketcher.Constraint('Angle', geo_lines[7], self.END, geo_lines[0], self.START, pi*5.0/180.0)
        ]
        con_param: tuple[int, ...] = self.sketch_0.addConstraint(parametric_constraints_input)
        self.doc.recompute()
        print('set_sketch_0 finished.')
        return


    def set_pad(self) -> None:
        self.pad = self.body_0.newObject('PartDesign::Pad','pad')
        self.pad.Profile = self.sketch_0
        self.pad.Length = 10.0
        self.pad.TaperAngle = 0.0
        self.pad.UseCustomVector = 0
        self.pad.Direction = (0, 0, 1)
        self.pad.ReferenceAxis = (self.sketch_0, ['N_Axis'])
        self.pad.AlongSketchNormal = 1
        self.pad.Type = 0
        self.pad.UpToFace = None
        self.pad.Reversed = 0
        self.pad.Midplane = 1
        self.pad.Offset = 0
        self.doc.recompute()
        print('set_pad finished.')
        return


    def set_sketch_1(self) -> None:
        self.sketch_1.Support = (self.doc.getObject('XY_Plane'),[''])
        self.sketch_1.MapMode = 'FlatFace'
        self.doc.recompute()

        cell: str = self.sheet.getCellFromAlias('wall_to_center')
        wall_to_center: float = self.sheet.get(cell)
        center: App.Vector = App.Vector(0.0, wall_to_center, 0.0)
        normal: App.Vector = App.Vector(0.0, 0.0, 1.0)

        self.sketch_1.addGeometry(Part.Circle(center, normal, self.radius),False) # '0' indicates this geometry
        self.sketch_1.addConstraint(Sketcher.Constraint('PointOnObject', 0, self.CENTER, self.Y_AXIS)) 
        self.sketch_1.addConstraint(Sketcher.Constraint('Distance', self.X_AXIS, self.START, 0, self.CENTER, wall_to_center)) 
        self.sketch_1.setExpression('Constraints[1]', 'sheet.wall_to_center')
        self.sketch_1.addConstraint(Sketcher.Constraint('Diameter', 0, 2.0 * self.radius))
        self.doc.recompute()
        print('set_sketch_1 finished.')
        return
    

    def set_pocket(self) -> None:
        self.pocket = self.body_0.newObject('PartDesign::Pocket','pocket')
        self.pocket.Profile = self.sketch_1
        self.pocket.ReferenceAxis = (self.sketch_1,['N_Axis'])
        self.pocket.Length = 10.0
        self.pocket.TaperAngle = 0.0
        self.pocket.UseCustomVector = 0
        self.pocket.Direction = (0, 0, -1)
        self.pocket.AlongSketchNormal = 1
        self.pocket.Type = 0
        self.pocket.UpToFace = None
        self.pocket.Reversed = 0
        self.pocket.Midplane = 1
        self.pocket.Offset = 0
        self.doc.recompute()
        print('set_pocket finished.')
        return


    def set_sketch_2(self) -> None:
        self.sketch_2.Support = (self.doc.getObject('XY_Plane001'),[''])
        self.sketch_2.MapMode = 'FlatFace'
        self.doc.recompute()

        cell: str = self.sheet.getCellFromAlias('wall_to_center')
        wall_to_center: float = self.sheet.get(cell)
        center: App.Vector = App.Vector(0.0, wall_to_center, 0.0)
        normal: App.Vector = App.Vector(0.0, 0.0, 1.0)

        self.sketch_2.addGeometry(Part.Circle(center, normal, 2.0*self.radius),False) # '0' indicates this geometry
        self.sketch_2.addConstraint(Sketcher.Constraint('PointOnObject', 0, self.CENTER, self.Y_AXIS)) 
        self.sketch_2.addConstraint(Sketcher.Constraint('Distance', self.X_AXIS, self.START, 0, self.CENTER, wall_to_center)) 
        self.sketch_2.setExpression('Constraints[1]', 'sheet.wall_to_center')
        self.sketch_2.addConstraint(Sketcher.Constraint('Diameter', 0, 4.0*self.radius))
        self.doc.recompute()
        print('set_sketch_2 finished.')
        return
    

    def set_region_0(self) -> None:
        self.region_0 = self.body_1.newObject('PartDesign::Pad','region_0')
        self.region_0.Profile = self.sketch_2
        self.region_0.Length = 15.0 # larger than that of self.pad
        self.region_0.TaperAngle = 0.0
        self.region_0.UseCustomVector = 0
        self.region_0.Direction = (0, 0, 1)
        self.region_0.ReferenceAxis = (self.sketch_2, ['N_Axis'])
        self.region_0.AlongSketchNormal = 1
        self.region_0.Type = 0
        self.region_0.UpToFace = None
        self.region_0.Reversed = 0
        self.region_0.Midplane = 1
        self.region_0.Offset = 0
        self.doc.recompute()
        print('set_region_0 finished.')
        return
    
        
    def set_sketch_3(self) -> None:
        self.sketch_3.Support = (self.doc.getObject('XY_Plane001'), [''])
        self.sketch_3.MapMode = 'FlatFace'
        self.doc.recompute()

        cell: str = self.sheet.getCellFromAlias('wall_to_center')
        wall_to_center: float = self.sheet.get(cell)

        points: list[App.Vector] = [
            App.Vector(-10.0*self.radius, wall_to_center - 5.0*self.radius, 0.0),
            App.Vector( 10.0*self.radius, wall_to_center - 5.0*self.radius, 0.0),
            App.Vector( 10.0*self.radius, wall_to_center + 5.0*self.radius, 0.0),
            App.Vector(-10.0*self.radius, wall_to_center + 5.0*self.radius, 0.0)
        ]

        lines: list[Part.LineSegment] = [
            Part.LineSegment(points[0], points[1]),
            Part.LineSegment(points[1], points[2]),
            Part.LineSegment(points[2], points[3]),
            Part.LineSegment(points[3], points[0])
        ]

        geo_lines: tuple[int, ...] = self.sketch_3.addGeometry(lines, False)
        self.doc.recompute()

        nonparametric_constraints_input: list[Sketcher.Constraint] = [
            Sketcher.Constraint('Horizontal', geo_lines[0]),
            Sketcher.Constraint('Vertical',   geo_lines[1]),
            Sketcher.Constraint('Horizontal', geo_lines[2]),
            Sketcher.Constraint('Vertical',   geo_lines[3]),
            Sketcher.Constraint('Coincident', geo_lines[1], self.START, geo_lines[0], self.END),
            Sketcher.Constraint('Coincident', geo_lines[2], self.START, geo_lines[1], self.END),
            Sketcher.Constraint('Coincident', geo_lines[3], self.START, geo_lines[2], self.END),
            Sketcher.Constraint('Coincident', geo_lines[0], self.START, geo_lines[3], self.END)
        ]
        con_nonparam: tuple[int, ...] = self.sketch_3.addConstraint(nonparametric_constraints_input)
        self.doc.recompute()

        parametric_constraints_input: list[Sketcher.Constraint] = [
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[0], abs(wall_to_center - 5.0*self.radius)),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[1], 10.0*self.radius),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[2], abs(wall_to_center + 5.0*self.radius)),
            Sketcher.Constraint('Distance', self.X_AXIS, self.START, geo_lines[3], 10.0*self.radius)
        ]
        con_param: tuple[int, ...] = self.sketch_3.addConstraint(parametric_constraints_input)
        self.doc.recompute()
        print('set_sketch_3 finished.')
        return
    

    def set_region_1(self) -> None:
        self.region_1 = self.body_1.newObject('PartDesign::Pad','region_1')
        self.region_1.Profile = self.sketch_3
        self.region_1.Length = 20.0 # larger than that of self.region_0
        self.region_1.TaperAngle = 0.0
        self.region_1.UseCustomVector = 0
        self.region_1.Direction = (0, 0, 1)
        self.region_1.ReferenceAxis = (self.sketch_3, ['N_Axis'])
        self.region_1.AlongSketchNormal = 1
        self.region_1.Type = 0
        self.region_1.UpToFace = None
        self.region_1.Reversed = 0
        self.region_1.Midplane = 1
        self.region_1.Offset = 0
        self.doc.recompute()
        print('set_region_1 finished.')
        return
    

    def export_stl(self) -> None:
        faces_outlet: list[Part.Face] = []
        faces_noslip: list[Part.Face] = []
        faces_slip  : list[Part.Face] = []
        faces_empty : list[Part.Face] = []

        tolerance: float = 1e-2 # mm
        faces: list[Part.Face] = self.body_0.Shape.Faces

        for face in faces:
            x: float = face.CenterOfMass.x
            y: float = face.CenterOfMass.y
            z: float = face.CenterOfMass.z
            if abs(x - self.x_min) < tolerance:
                face_inlet: Part.Face = face
            elif abs(x - self.x_max) < tolerance:
                faces_outlet.append(face)
            elif abs(y - self.y_max) < tolerance:
                faces_noslip.append(face)
            elif abs(y - self.y_min) < tolerance:
                faces_slip.append(face)
            elif abs(z - 5.0) < tolerance:
                faces_empty.append(face)
            elif abs(z + 5.0) < tolerance:
                faces_empty.append(face)
            elif abs(y) < tolerance:
                faces_noslip.append(face)
            elif abs(x) < tolerance:
                face_cylinder: Part.Face = face
            else:
                faces_slip.append(face)

        face_outlet: Part.Compound = Part.Compound(faces_outlet)
        face_noslip: Part.Compound = Part.Compound(faces_noslip)
        face_slip  : Part.Compound = Part.Compound(faces_slip)
        face_empty : Part.Compound = Part.Compound(faces_empty)
        self.doc.recompute()

        os.makedirs(f'{self.dir_st1}/stl', exist_ok=True)

        face_inlet   .exportStl(f'{self.dir_st1}/stl/inlet.ast')
        face_outlet  .exportStl(f'{self.dir_st1}/stl/outlet.ast')
        face_cylinder.exportStl(f'{self.dir_st1}/stl/cylinder.ast')
        face_noslip  .exportStl(f'{self.dir_st1}/stl/noslip.ast')
        face_slip    .exportStl(f'{self.dir_st1}/stl/slip.ast')
        face_empty   .exportStl(f'{self.dir_st1}/stl/empty.ast') # cartesian2DMesh doesn't require the empty faces.

        self.region_0.Shape.exportStl(f'{self.dir_st1}/stl/region_0.ast')
        self.region_1.Shape.exportStl(f'{self.dir_st1}/stl/region_1.ast')
        self.doc.recompute()
        print('export_stl finished.')
        return
    

    def save(self) -> None:
        self.doc.saveAs(f'{self.dir_st1}/cylinder_near_a_wall.FCStd')
        return
    
    
c = CreateStl()

c.set_sheet()

c.set_sketch_0()
c.set_pad()

c.set_sketch_1()
c.set_pocket()

c.set_sketch_2()
c.set_region_0()

c.set_sketch_3()
c.set_region_1()

c.export_stl()

c.save()
