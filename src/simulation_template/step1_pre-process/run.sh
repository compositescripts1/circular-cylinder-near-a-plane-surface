#!/bin/bash
cd "${0%/*}" || exit

dir_step1=$(pwd)                # /src/simulation_template/step1_pre-process
dir_tmp=$(dirname "$dir_step1") # /src/simulation_template
dir_src=$(dirname "$dir_tmp")   # /src

# Source PowerShell scripts to activate miniconda
. "$dir_src"/miniconda/initialize_miniconda.sh
. "$dir_src"/miniconda/activate_opencae.sh

freecadcmd fc_createstl.py

cd "$dir_step1"/stl || exit
find *.ast | sed "s/\.ast//" | xargs -I@ mv @.ast @.stl
find *.stl | sed "s/\.stl//" | xargs -I@ sed -i "s/solid/solid @/g" @.stl # DO NOT WRITE AS" find ./*.stl " OR @ CONTAINS "/".
cat inlet.stl outlet.stl cylinder.stl noslip.stl slip.stl > geometry.stl

cd "$dir_step1" || exit
