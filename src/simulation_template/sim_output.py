import json
from dataclasses import dataclass
from dataclasses_json import LetterCase, dataclass_json


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class SimOutput:
    """Output data class for simulation by OpenFOAM.
            
        the json file looks like:
        {
            "objectiveVariable": 0.9,
            "numberOfTrial": 1 // in this case, it's the first simulation 
        }
    """
    objective_variable: float
    number_of_trial: int
