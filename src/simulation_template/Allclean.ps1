$dir_sim = $PSScriptRoot        # /src/simulation_template/
Set-Location $dir_sim || exit   # Run from this directory
#------------------------------------------------------------------------------

Remove-Item sim_output.json -ErrorAction SilentlyContinue

Set-Location step1_pre-process || exit
./clean.ps1

Set-Location ../step2_solve || exit
./clean.ps1

Set-Location ../step3_post-process || exit
./clean.ps1


Set-Location $dir_sim || exit
