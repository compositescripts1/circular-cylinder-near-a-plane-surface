from PySide6.QtWidgets import *


class Tab1Objective(QWidget):
    def __init__(self):
        super().__init__()

        optimization_objective:   QGroupBox = self.create_optimization_objective()
        optimization_constraints: QGroupBox = self.create_optimization_constraints()

        main_layout = QVBoxLayout()
        main_layout.addWidget(optimization_objective)
        main_layout.addWidget(optimization_constraints)
        main_layout.addStretch(1)
        self.setLayout(main_layout)
        return


    def create_optimization_objective(self) -> QGroupBox:
        optimization_objective = QGroupBox('1.1 Objective of optimization                                    ')

        self.combobox = QComboBox()
        self.combobox.addItems(
            [
                'Maximize drag coefficient',
                'Maximize lift coefficient'
            ]
        )

        layout = QVBoxLayout()
        layout.addWidget(self.combobox)

        optimization_objective.setLayout(layout)
        return optimization_objective


    def create_optimization_constraints(self) -> QGroupBox:
        optimization_constraints = QGroupBox('1.2 Constraints of optimization                                    ')

        label = QLabel('There is no constraints.')

        layout = QVBoxLayout()
        layout.addWidget(label)

        optimization_constraints.setLayout(layout)
        return optimization_constraints


    def creare_boundary_layer_setting(self):
        return