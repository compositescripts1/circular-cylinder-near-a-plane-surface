import os
import subprocess
import sys
from PySide6.QtWidgets import *
from PySide6.QtGui import QFont

# my own classes
dir_pyside = os.path.dirname(__file__)
dir_src = os.path.dirname(dir_pyside)
dir_optuna = f'{dir_src}/optuna'
sys.path.append(dir_optuna)
from input import *


class Tab2DesignVariables(QWidget):
    def __init__(self):
        super().__init__()

        parameters_spinbox: QGroupBox = self.create_parameters_spinbox()
        cad_displayer     : QGroupBox = self.create_cad_displayer()

        main_layout = QVBoxLayout()
        main_layout.addWidget(parameters_spinbox)
        main_layout.addWidget(cad_displayer)
        main_layout.addStretch(1)
        self.setLayout(main_layout)
        return


    def create_parameters_spinbox(self) -> QGroupBox:
        d = DesignVariableNames()

        parameters_spinbox = QGroupBox('2.1 Range of parameters')

        label_min = QLabel("Lower bound")
        label_max = QLabel("Upper bound")

        labels: list[QLabel] = []
        for i in range(len(d.names)):
            labels.append(QLabel(d.names[i]))

        self.spinboxes_min: list[QDoubleSpinBox] = []
        for i in range(len(d.names)):
            spinbox_min = QDoubleSpinBox()
            spinbox_min.setMinimum(-1000.0)
            spinbox_min.setMaximum(1000.0)
            spinbox_min.setValue(0.01)
            self.spinboxes_min.append(spinbox_min)

        self.spinboxes_max: list[QDoubleSpinBox] = []
        for i in range(len(d.names)):
            spinbox_max = QDoubleSpinBox()
            spinbox_max.setMinimum(-1000.0)
            spinbox_max.setMaximum(1000.0)
            spinbox_max.setValue(999.99)
            self.spinboxes_max.append(spinbox_max)

        layout = QGridLayout()
        layout.addWidget(label_min,                 0,  1,  1,  1)
        layout.addWidget(label_max,                 0,  2,  1,  1)
        for i in range(len(d.names)):
            layout.addWidget(labels[i],             i+1,0,  1,  1)
            layout.addWidget(self.spinboxes_min[i], i+1,1,  1,  1)
            layout.addWidget(self.spinboxes_max[i], i+1,2,  1,  1)

        parameters_spinbox.setLayout(layout)
        return parameters_spinbox


    def create_cad_displayer(self) -> QGroupBox:
        cad_displayer = QGroupBox('2.2 (Optional) Confirm geometry')

        button = QCommandLinkButton('Open FreeCAD file')
        button.setDefault(False)        # not to be clicked by Enter key
        button.setAutoDefault(False)    # not to be clicked by Enter key
        font: QFont = button.font()
        font.setPointSize(14)
        button.setFont(font)
        button.clicked.connect(self.open_freecadfile)

        layout = QHBoxLayout()
        layout.addWidget(button)

        cad_displayer.setLayout(layout)
        return cad_displayer


    def open_freecadfile(self) -> None:
        pwd: str = os.path.dirname(__file__) # ./src/pyside/
        src: str = os.path.dirname(pwd)      # ./src/
        src = src.replace('\\', '/')
        
        if os.name == 'nt':
            show_freecad_ps1: str = f'{src}/simulation_template/step1_pre-process/show_freecad.ps1'
            p = subprocess.Popen(["powershell.exe", show_freecad_ps1], stdout=sys.stdout)
            p.communicate()
        elif os.name == 'posix':
            show_freecad_sh: str = f'{src}/simulation_template/step1_pre-process/show_freecad.sh'
            p = subprocess.Popen(["bash", show_freecad_sh], stdout=sys.stdout)
            p.communicate()
        
        return