import datetime
import glob
import os
import re
import subprocess
import sys

from PySide6.QtWidgets import *
from PySide6.QtCore import QDir, QModelIndex
from PySide6.QtGui import QFont

# my own classes
from tab1_objective import Tab1Objective
from tab2_designvariables import Tab2DesignVariables

dir_pwd = os.path.dirname(__file__)
dir_src = os.path.dirname(dir_pwd)
sys.path.append(f"{dir_src}/optuna")
from input import *


class Tab3Execution(QWidget):
    selected_file = ''

    def __init__(self, tab1, tab2):
        super().__init__()
        self.design_variable_names = DesignVariableNames()
        
        self.times_run_button_clicked: int = 0
            
        self.dir_pwd : str = os.path.dirname(__file__)     # ./src/pyside/
        self.dir_src : str = os.path.dirname(self.dir_pwd) # ./src/
        self.dir_root: str = os.path.dirname(self.dir_src) # ./
        self.dir_src  = self.dir_src.replace('\\', '/')
        self.dir_root = self.dir_root.replace('\\', '/')

        self.tab1: Tab1Objective = tab1
        self.tab2: Tab2DesignVariables = tab2

        number_of_trials     : QGroupBox = self.create_number_of_trials()
        new_optimization     : QGroupBox = self.create_new_optimization()
        resuming_optimization: QGroupBox = self.create_resuming_optimization()
        force_stop           : QGroupBox = self.create_force_stop()

        main_layout = QVBoxLayout()
        main_layout.addWidget(number_of_trials)
        main_layout.addWidget(new_optimization)
        main_layout.addWidget(resuming_optimization)
        main_layout.addWidget(force_stop)
        main_layout.addStretch(1)

        self.setLayout(main_layout)
        return


    def create_number_of_trials(self) -> QGroupBox:
        number_of_trials = QGroupBox('3.1 Number of Trials (up to 100. in resuming, this means number of APPENDED trials)')

        self.spinbox = QSpinBox()
        self.spinbox.setMaximum(100)
        self.spinbox.setValue(30)

        layout = QVBoxLayout()
        layout.addWidget(self.spinbox)

        number_of_trials.setLayout(layout)
        return number_of_trials
    

    def create_new_optimization(self) -> QGroupBox:
        new_optimization = QGroupBox('3.3 Execute New Optimization')

        run_button = QCommandLinkButton("Run (and Browse the Results)")
        run_button.setDefault(False)        # not to be clicked by Enter key
        run_button.setAutoDefault(False)    # not to be clicked by Enter key
        font: QFont = run_button.font()
        font.setPointSize(14)
        run_button.setFont(font)
        run_button.clicked.connect(self.execute_new_optimization)

        layout = QVBoxLayout()
        layout.addWidget(run_button)

        new_optimization.setLayout(layout)
        return new_optimization


    def create_resuming_optimization(self) -> QGroupBox:
        resuming_optimization = QGroupBox('3.4 Resume Existing Optimization')

        self.model = QFileSystemModel()
        rootpath = QDir.toNativeSeparators(self.dir_root)
        self.model.setRootPath(rootpath) # not work as I expected...

        self.file_tree = QTreeView()
        self.file_tree.setModel(self.model)
        self.file_tree.clicked.connect(self.show_selected_file)

        self.selected_file_label = QLabel('Select the .db file...')
        
        run_button = QCommandLinkButton("Run (and Browse the Results)")
        run_button.setDefault(False)        # not to be clicked by Enter key
        run_button.setAutoDefault(False)    # not to be clicked by Enter key
        font: QFont = run_button.font()
        font.setPointSize(14)
        run_button.setFont(font)
        run_button.clicked.connect(self.execute_existing_optimization)

        browse_button = QCommandLinkButton("Just Browse the Results")
        browse_button.setDefault(False)        # not to be clicked by Enter key
        browse_button.setAutoDefault(False)    # not to be clicked by Enter key
        font: QFont = browse_button.font()
        font.setPointSize(14)
        browse_button.setFont(font)
        browse_button.clicked.connect(self.open_optuna_dashboard)

        layout = QGridLayout()
        layout.addWidget(self.file_tree,            0,0, 1,2)
        layout.addWidget(self.selected_file_label,  1,0, 1,2)
        layout.addWidget(run_button,                2,0, 1,1)
        layout.addWidget(browse_button,             2,1, 1,1)

        resuming_optimization.setLayout(layout)
        return resuming_optimization


    def create_force_stop(self) -> QGroupBox:
        force_stop = QGroupBox()

        stop_button = QCommandLinkButton("Force Stop")
        stop_button.setDefault(False)        # not to be clicked by Enter key
        stop_button.setAutoDefault(False)    # not to be clicked by Enter key
        font: QFont = stop_button.font()
        font.setPointSize(14)
        stop_button.setFont(font)
        stop_button.clicked.connect(self.force_stop_optimization)

        layout = QVBoxLayout()
        layout.addWidget(stop_button)

        force_stop.setLayout(layout)
        return force_stop
    

    def show_selected_file(self, index) -> None:
        indexItem: QModelIndex = self.model.index(index.row(), 0, index.parent())
        self.selected_file: str = self.model.filePath(indexItem)
        self.selected_file_label.setText(f"Selecting ' {self.selected_file} '.") # signal-slot
        return


    def execute_new_optimization(self) -> None:
        self.times_run_button_clicked += 1

        if self.times_run_button_clicked == 1:
            # Create results folder    
            tmp: datetime.datetime = datetime.datetime.now()
            self.datetime_now = '{0:%Y-%m-%dT%H%M}'.format(tmp) # ISO8601 anomaly
            dir_results: str = f'{self.dir_root}/results-{self.datetime_now}'
            os.makedirs(dir_results, exist_ok=True)
        
            self.write_input_json(dir_results)            
            self.run_optuna('new') #  Windowsの場合，非同期で実行できていない!!!なぜ？
        else:
            self.popup_do_not_run_twice()
        return


    def execute_existing_optimization(self) -> None:
        if re.search(r'\.db$', self.selected_file) == None: # NOT WORK!!! dot seems to be a separater of the string
            self.popup_not_found_dbfile()
        else:
            self.times_run_button_clicked += 1

            if self.times_run_button_clicked == 1:
                # Get results folder    
                dir_results: str = os.path.dirname(self.selected_file)        
                self.rewrite_input_json(dir_results)            
                self.run_optuna('resume') #  Windowsの場合，非同期で実行できていない!!!なぜ？
            else:
                self.popup_do_not_run_twice()
        return


    def write_input_json(self, dir_results: str) -> None:
        """Create json file which optuna needs as input
            see: /src/optuna/input.py
        """
        names: list[str] = self.design_variable_names.names

        _variables: dict[str, list[float]] = {}
        for i in range(len(names)):
            _variables[names[i]] = [
                self.tab2.spinboxes_min[i].value(), 
                self.tab2.spinboxes_max[i].value()
            ]

        input = Input(
            objective_of_optimization = self.tab1.combobox.currentText(),
            design_variables = _variables,
            number_of_trials = self.spinbox.value()
        )

        with open(f'{dir_results}/input.json', 'w', encoding='utf-8') as file:
            file.write(input.to_json(ensure_ascii=False, indent=4))
        return


    def rewrite_input_json(self, dir_results: str) -> None:
        """Modify json file which optuna needs as input
            see: /src/optuna/input.py
            for resume the optimization, only number_of_trials is modified.
        """
        with open(f'{dir_results}/input.json', 'r', encoding='utf-8') as file:
            input_json: dict =  json.load(file)
                
        input: Input = Input.from_json(str(input_json).replace('\'', '"'))
        input.number_of_trials = self.spinbox.value()
        
        with open(f'{dir_results}/input.json', 'w', encoding='utf-8') as file:
            file.write(input.to_json(ensure_ascii=False, indent=4))
        return


    def run_optuna(self, keyword: str) -> None:
        """
        keyword = (new|resume)
        """
        if keyword == 'new':
            value: str = self.datetime_now  # timestamp of creation
        elif keyword == 'resume':
            value: str = self.selected_file # fullpath of existing db file

        if os.name == 'nt':
            # BUG: this process should be done asynchronous, but actually done synchronous!!!
            run_optuna_ps1: str = f'{self.dir_src}/run_optuna.ps1'
            p = subprocess.Popen(["powershell.exe", run_optuna_ps1, keyword, value], stdout=sys.stdout)
            p.communicate()
        elif os.name == 'posix':
            run_optuna_sh: str = f'{self.dir_src}/run_optuna.sh'
            p = subprocess.Popen(["bash", run_optuna_sh, keyword, value], stdout=sys.stdout)
            p.communicate()
        return
    
    
    def open_optuna_dashboard(self) -> None:        
        if re.search(r'\.db$', self.selected_file) == None: # NOT WORK!!! dot seems to be a separater of the string
            self.popup_not_found_dbfile()
        else:
            if os.name == 'nt':
                open_dashboard_ps1: str = f'{self.dir_src}/optuna/open_dashboard.ps1'
                p = subprocess.Popen(["powershell.exe", open_dashboard_ps1, self.selected_file], stdout=sys.stdout)
                p.communicate()
            elif os.name == 'posix':
                open_dashboard_sh: str = f'{self.dir_src}/optuna/open_dashboard.sh'
                p = subprocess.Popen(["bash", open_dashboard_sh, self.selected_file], stdout=sys.stdout)
                p.communicate()
        return


    def force_stop_optimization(self) -> None:
        resbtn = QMessageBox.question(
            self,
            'CONFIRMATION',
            'Are you sure about force-stopping this optimization? trial data will be lost.',
            QMessageBox.No | QMessageBox.Yes,
            QMessageBox.Yes
        )
        if resbtn == QMessageBox.Yes:
            self.kill_optuna()
        return

    
    def kill_optuna(self) -> None:
        if os.name == 'nt':
            kill_optuna_ps1: str = f'{self.src}/optuna/kill_optuna.ps1'
            p = subprocess.Popen(["powershell.exe", kill_optuna_ps1], stdout=sys.stdout)
            p.communicate()
        elif os.name == 'posix':
            kill_optuna_sh: str = f'{self.src}/optuna/kill_optuna.sh'
            p = subprocess.Popen(["bash", kill_optuna_sh], stdout=sys.stdout)
            p.communicate()        
        return
    

    def popup_not_found_dbfile(self) -> None:
        self.popup = QMessageBox()
        self.popup.setWindowTitle('ERROR')

        message: str = ""
        message +=  "No .db file is found! \n"
        message += f"selected file: {self.selected_file} \n"
        message +=  "Confirm you've selected the proper file. \n"
        
        self.popup.setText(message)
        self.popup.show()
        return
    

    def popup_do_not_run_twice(self) -> None:
        self.popup = QMessageBox()
        self.popup.setWindowTitle('WARNING')

        message: str = ''
        message += 'You cannot run the optimization twice.\n'
        message += 'Restart this wizard and try again. \n'
        
        self.popup.setText(message)
        self.popup.show()
        return
