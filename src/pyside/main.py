import os
import subprocess
import sys

from PySide6.QtWidgets import *
from PySide6.QtGui import QDesktopServices, QFont
from PySide6.QtCore import QUrl

from tab1_objective import Tab1Objective 
from tab2_designvariables import Tab2DesignVariables
from tab3_execution import Tab3Execution


class MainFrame(QDialog):
    def __init__(self):
        super().__init__()
        
        self.pwd: str = os.path.dirname(__file__) # ./src/pyside/
        self.src: str = os.path.dirname(self.pwd) # ./src/
        self.src: str = self.src.replace('\\', '/')

        self.setWindowTitle('Optimize a fluid force acting on a circular cylinder near a wall')

        tabwidget: QTabWidget = self.create_tabwidget()
        dialog_buttonbox: QDialogButtonBox = self.create_dialog_buttonbox()

        main_layout = QVBoxLayout(self)
        main_layout.addWidget(tabwidget)
        main_layout.addWidget(dialog_buttonbox)
        return
    

    def create_tabwidget(self) -> QTabWidget:
        _tabwidget = QTabWidget()

        objective_tab = Tab1Objective() # maybe need 'self.' ?
        parameter_tab = Tab2DesignVariables()
        execution_tab = Tab3Execution(objective_tab, parameter_tab)

        _tabwidget.addTab(objective_tab, '1. Objectives/Constraints')
        _tabwidget.addTab(parameter_tab, '2. Design Variables')
        _tabwidget.addTab(execution_tab, '3. Execution')
        return _tabwidget

    
    def create_dialog_buttonbox(self) -> QDialogButtonBox:
        _dialog_buttonbox = QDialogButtonBox( QDialogButtonBox.Help )
        _dialog_buttonbox.buttons()[0].setDefault(False)        # not to be clicked by Enter key.
        _dialog_buttonbox.buttons()[0].setAutoDefault(False)    # not to be clicked by Enter key.
        _dialog_buttonbox.helpRequested.connect(self.launch_help)
        return _dialog_buttonbox


    def launch_help(self) -> None:
        file: str = f'{self.src}/help/help.html'
        QDesktopServices.openUrl(QUrl(file))
        return

    
    def close_dashboard(self) -> None:
        if os.name == 'nt':
            close_dashboard_ps1: str = f'{self.src}/optuna/close_dashboard.ps1'
            p = subprocess.Popen(["powershell.exe", close_dashboard_ps1], stdout=sys.stdout)
            p.communicate()
        elif os.name == 'posix':
            close_dashboard_sh: str = f'{self.src}/optuna/close_dashboard.sh'
            p = subprocess.Popen(["bash", close_dashboard_sh], stdout=sys.stdout)
            p.communicate()        
        return


    def reject(self) -> None:
        resbtn = QMessageBox.question(
            self,
            'CONFIRMATION',
            'Are you sure about terminating this wizard?',
            QMessageBox.No | QMessageBox.Yes,
            QMessageBox.Yes
        )
        if resbtn == QMessageBox.Yes:
            self.close_dashboard()
            super().reject()
        return


if __name__ == '__main__':
    app = QApplication(sys.argv)

    app.setStyle('fusion')

    font: QFont = app.font()
    font.setPointSize(11)
    app.setFont(font)

    wizard = MainFrame()
    wizard.show()
    sys.exit(app.exec())
    