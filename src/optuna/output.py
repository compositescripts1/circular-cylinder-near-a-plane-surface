import json
from dataclasses import dataclass
from dataclasses_json import LetterCase, dataclass_json


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Output:
    """summarize optimization. maybe no use( because of optuna-dashboard's useful feature  )
    """

    ### NEED TO BE MODIFIED
    # description: str
    # objective_of_optimization: str
    # design_variable: dict[str, list[float]]
    # # There is 1 key: 'c: Clearance/Diameter'
    # number_of_trials: int
