#!/bin/bash
cd "${0%/*}" || exit

dir_optuna=$(pwd)                # /src/optuna
dir_src=$(dirname "$dir_optuna") # /src/

file_path_db=$1
dir_db=$(dirname "$file_path_db")
file_name_db=$(basename "$file_path_db")

cd "$dir_db" || exit

# Source shell scripts to activate miniconda
. "$dir_src"/miniconda/initialize_miniconda.sh
. "$dir_src"/miniconda/activate_opencae.sh

optuna-dashboard sqlite:///"$file_name_db" & # need to be asynchronous to open browser.
sleep 5
xdg-open http://127.0.0.1:8080/

# Return to the directory where we were
cd "$dir_optuna" || exit
