import json
from dataclasses import dataclass
from dataclasses_json import LetterCase, dataclass_json


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class Input:
    """Input data class for optimization by optuna.
        We need json file first, then transrate it to this dataclass.
        We can create the json file by hand or by pyside app. see:
            /src/pyside/tab3_execution.py -> Tab3Execution.write_input_json() 
            
        the json file looks like:
        {
            "objectiveOfOptimization": "Maximize drag coefficient",
            "designVariables": {
                "Clearance/Diameter": [
                    0.1,
                    4.0
                ],
                "variableName": [
                    min,
                    max
                ]
            },
            "numberOfTrials": 20
        }
    """
    objective_of_optimization: str
    design_variables: dict[str, list[float]]
    number_of_trials: int


class DesignVariableNames:
    """
    Define names of design variables. 
    This is the reference of all manipulation about design variable names.
    """
    names: list[str]

    names = [
        'Clearance/Diameter',
        'Diameter*AngVelocity/Velocity'
    ]
    # In this case, there is only one key: 'c: Clearance/Diameter'
