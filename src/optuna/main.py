import logging
import os
import subprocess
import sys
from time import sleep
import optuna

# my own classes
from input import *
dir_optuna = os.path.dirname(__file__)
dir_src = os.path.dirname(dir_optuna)
dir_sim = f'{dir_src}/simulation_template'
sys.path.append(dir_sim)
from sim_input import *
from sim_output import *


class Optimize():
    """
    For modifiers:
        To customize this code for your aim, you can modify these functions.
        
        To modify OBJECTIVE FUNCTION:
        - objective()                   # this function contains these 3 functions.
            - write_sim_input_json()
            - run_simulation()
            - copy_logs()

        To export VISUALIZATION RESULTS of optimization:
        - export_visualization()
    """
    isDummy = False      # True | False

    dir_optuna: str  = os.path.dirname(__file__)            # /src/optuna/
    dir_src: str     = os.path.dirname(dir_optuna)          # /src/
    dir_root: str    = os.path.dirname(dir_src)             # /
    dir_results: str                                        # /results-XXX


    def __init__(self) -> None:        
        # Create new study/Resume study
        self._study_name: str
        if sys.argv[1] == 'new':
            self.datetime_now: str = sys.argv[2]
            self.dir_results = f'{self.dir_root}/results-{self.datetime_now}'
            self._study_name = f'cylinder-near-a-surface-{self.datetime_now}'
        elif sys.argv[1] == 'resume':
            path_dbfile: str = sys.argv[2]
            self.dir_results = os.path.dirname(path_dbfile)
            self._study_name = os.path.basename(path_dbfile).replace('.db', '')
        else:
            print('Wrong keyword!!!')
            exit

        optuna.logging.get_logger("optuna").addHandler(logging.StreamHandler(sys.stdout))
        _storage: str = f"sqlite:///{self._study_name}.db"
        self.study: optuna.study.study.Study = optuna.create_study(study_name=self._study_name, storage=_storage, direction="maximize", load_if_exists=True) # Maximize the objective function.
        
        # Read inputs
        self.design_variable_names = DesignVariableNames()
        self.read_input_json()
        return
    

    def read_input_json(self) -> None:
        with open(f'{self.dir_results}/input.json', 'r', encoding='utf-8') as file:
            input_json: dict =  json.load(file)
                
        self.input: Input = Input.from_json(str(input_json).replace('\'', '"'))
        return
    

    def run(self) -> None:
        ### Open optuna-dashboard in browser
        if os.name == 'nt':
            open_dashboard_ps1: str = f'{self.dir_optuna}/open_dashboard.ps1'
            file_path_db: str = f'{self.dir_results}/{self._study_name}.db'
            p: subprocess.Popen = subprocess.Popen(["powershell.exe", open_dashboard_ps1, file_path_db], stdout=sys.stdout)
            p.communicate()
        elif os.name == 'posix':
            open_dashboard_sh: str = f'{self.dir_optuna}/open_dashboard.sh'
            file_path_db: str = f'{self.dir_results}/{self._study_name}.db'
            p: subprocess.Popen = subprocess.Popen(["bash", open_dashboard_sh, file_path_db], stdout=sys.stdout)
            p.communicate()
        
        ### Run
        if self.isDummy:
            self.study.optimize(self.objective_dummy, n_trials=self.input.number_of_trials)
        else:
            self.study.optimize(self.objective, n_trials=self.input.number_of_trials)
        
        print('optimization finished.')
        return


    def objective_dummy(self, trial: optuna.trial.Trial) -> float:
        """
        Dummy objective function to demonstrate the whole system in short time.
        """
        # sleep(1)
        names: list[str] = self.design_variable_names.names
        x: float = trial.suggest_float("x", self.input.design_variables[names[0]][0], self.input.design_variables[names[0]][1])
        # y: int   = trial.suggest_categorical("y", [-1, 0, 1])
        y: float = trial.suggest_float("y", self.input.design_variables[names[1]][0], self.input.design_variables[names[1]][1])
        return x**2 + y


    def objective(self, trial: optuna.trial.Trial) -> float:
        """
        This function returns OBJECTIVE FUNCTION.
        In this case, it is drag/lift coefficient.

        to be archetype of various optimization, 'variables' is made to be dictionary 
        (though it has only one element in this case).
        """
        names: list[str] = self.design_variable_names.names

        # set design variables of single trial. 
        variables: dict[str, float] = {}
        for i in range(len(names)):
            variables[names[i]] = trial.suggest_float(
                names[i], 
                self.input.design_variables[names[i]][0], 
                self.input.design_variables[names[i]][1]
            )

        # RUN OPENFOAM. GET DRAG OR LIFT
        self.write_sim_input_json(variables, trial.number)
        forcecoeff: float = self.run_simulation()
        self.copy_logs(trial.number)

        return forcecoeff
    

    def write_sim_input_json(self, variables: dict[str, float], i: int) -> None:
        """Create json file which OpenFOAMs needs as input
            see: /src/simulation_template/sim_input.py
        """
        sim_input = SimInput(
            objective_of_optimization = self.input.objective_of_optimization,
            design_variables = variables,
            number_of_trial = i
        )

        with open(f'{self.dir_src}/simulation_template/sim_input.json', 'w', encoding='utf-8') as file:
            file.write(sim_input.to_json(ensure_ascii=False, indent=4))
        return
    

    def run_simulation(self) -> float:
        ### RUN OPENFOAM SIMULATION
        if os.name == 'nt':
            clean_simulation_ps1: str = f'{self.dir_src}/simulation_template/Allclean.ps1'
            run_simulation_ps1: str = f'{self.dir_src}/simulation_template/Allrun.ps1'
            p0: subprocess.run = subprocess.run(["pwsh", clean_simulation_ps1], stdout=sys.stdout)
            p1: subprocess.run = subprocess.run(["pwsh", run_simulation_ps1], stdout=sys.stdout)
        elif os.name == 'posix':
            clean_simulation_sh: str = f'{self.dir_src}/simulation_template/Allclean.sh'
            run_simulation_sh: str = f'{self.dir_src}/simulation_template/Allrun.sh'
            p0: subprocess.run = subprocess.run(["bash", clean_simulation_sh], stdout=sys.stdout)
            p1: subprocess.run = subprocess.run(["bash", run_simulation_sh], stdout=sys.stdout)

        ### READ OUTPUT JSON
        with open(f'{self.dir_src}/simulation_template/sim_output.json', 'r', encoding='utf-8') as file:
            output_json: dict =  json.load(file)
                
        self.sim_output: SimOutput = SimOutput.from_json(str(output_json).replace('\'', '"'))
        
        return self.sim_output.objective_variable


    def copy_logs(self, i: int) -> None:
        dir_st3: str = f'{self.dir_src}/simulation_template/step3_post-process'
        dir_png: str = f'{dir_st3}/png'
        number = str(i).zfill(3)
        os.makedirs(f'{self.dir_results}/{number}/log', exist_ok=True)

        if os.name == 'nt':
            dir_st3 = dir_st3.replace('/', '\\')
            dir_png = dir_png.replace('/', '\\')
            dir_results = self.dir_results.replace('/', '\\')
            
            p = subprocess.run(f"copy {dir_st3}\log* {dir_results}\{number}\log", shell=True, stdout=sys.stdout) # cmd commands 'copy'. (not powershell's 'Copy-Item')
            p = subprocess.run(f"copy {dir_png}\* {dir_results}\{number}", shell=True, stdout=sys.stdout) # cmd commands 'copy'. (not powershell's 'Copy-Item')
        elif os.name == 'posix':
            p = subprocess.run(f"cp {dir_st3}/log* {self.dir_results}/{number}/log", shell=True, stdout=sys.stdout)
            p = subprocess.run(f"cp {dir_png}/* {self.dir_results}/{number}", shell=True, stdout=sys.stdout)
        return
    

    def export_visualization(self) -> None:
        fig0 = optuna.visualization.plot_optimization_history(self.study)
        fig0.write_html(f'{self.dir_results}/history.html')

        fig1 = optuna.visualization.plot_slice(self.study, params=[self.design_variable_names.names[0]])
        fig1.write_html(f'{self.dir_results}/slice_param0.html')

        print('has exported visualization.')
        return


if __name__ == "__main__":
    o = Optimize()
    o.run()
    o.export_visualization()

