param(
    [Parameter()]
    [String]$file_path_db
)

$dir_pwd = Get-Location

$dir_optuna = $PSScriptRoot # /src/optuna
$dir_src = Split-Path $dir_optuna                     # /src/

$dir_db = Split-Path $file_path_db
$file_name_db = Split-Path $file_path_db -Leaf

Write-Host $dir_db # for debugging
Write-Host $file_name_db

# Source PowerShell scripts to activate miniconda
. $dir_src/miniconda/initialize_miniconda.ps1
. $dir_src/miniconda/activate_opencae.ps1

Set-Location $dir_db


$show_window = $false    # $true: showing prompts for debug. | $false: no prompts.
if ($show_window) {
    Start-Process -FilePath "optuna-dashboard" -ArgumentList "sqlite:///$file_name_db"
    Start-Sleep 5
    Start-Process -FilePath "msedge" -ArgumentList "http://127.0.0.1:8080/"
} else {
    Start-Process -WindowStyle hidden -FilePath "optuna-dashboard" -ArgumentList "sqlite:///$file_name_db"
    Start-Sleep 5
    Start-Process -WindowStyle hidden -FilePath "msedge" -ArgumentList "http://127.0.0.1:8080/"
}

# Return to the directory where we were
Set-Location $dir_pwd
