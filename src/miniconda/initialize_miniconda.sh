#!/bin/bash

# Check if Miniconda env 'opencae' has been created
# set -eu
set -e
set +u # just to ignore an error(should be fixed?)

catch () {
    error_message=""
    error_message+="Miniconda is not installed! \n"
    error_message+="Install Miniconda according to this page: \n"
    error_message+="    https://gitlab.com/create-environment/miniconda \n"
    echo "$error_message"
}

trap catch ERR

### main
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$("$HOME/miniconda3/bin/conda" 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "$HOME/miniconda3/etc/profile.d/conda.sh" ]; then
        . "$HOME/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="$HOME/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda init bash
