# Check if Miniconda has installed
try {
    & C:/ProgramData/miniconda3/shell/condabin/conda-hook.ps1
}
catch {
    try {
        & ${env:userprofile}/AppData/Local/miniconda3/shell/condabin/conda-hook.ps1
    }
    catch {
        $error_message = ''
        $error_message += "Miniconda is not installed! `r`n"
        $error_message += "Install Miniconda according to this page: `r`n"
        $error_message += "    https://gitlab.com/create-environment/miniconda `r`n"
        $error_message += "`r`n"
        $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

        Add-Type -AssemblyName System.Windows.Forms;
        [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
    }
}
