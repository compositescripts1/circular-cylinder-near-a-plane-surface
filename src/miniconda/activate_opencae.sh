#!/bin/bash

# Check if Miniconda env 'opencae' has been created
set -eu

catch () {
    error_message=""
    error_message+="There is no Miniconda environment named 'opencae'! \n"
    error_message+="Create the environment according to this page: \n"
    error_message+="    https://gitlab.com/create-environment/miniconda \n"
    echo "$error_message"
}

trap catch ERR

conda activate opencae
