# Check if Miniconda env 'opencae' has been created
try {
    conda activate opencae
}
catch {
    $error_message = ""
    $error_message += "There is no Miniconda environment named 'opencae'! `r`n"
    $error_message += "Create the environment according to this page: `r`n"
    $error_message += "    https://gitlab.com/create-environment/miniconda `r`n"
    $error_message += "`r`n"
    $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

    Add-Type -AssemblyName System.Windows.Forms;
    [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
}
