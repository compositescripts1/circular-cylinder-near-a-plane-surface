param(
    [Parameter()]
    [String]$keyword,               # "new" | "resume"
    [String]$value                  # e.g. keyword = "new"    : value = "2023-06-12T1557" 
                                    #      keyword = "resume" : value = "C:\_GitLab\composite\circular-cylinder-near-a-plane-surface\results-2023-06-12T1557\cylinder-near-a-surface-2023-06-12T1557.db"
)

$dir_src = $PSScriptRoot            # /src
$dir_root = Split-Path $dir_src     # /

# Source PowerShell scripts to activate miniconda
. $dir_src/miniconda/initialize_miniconda.ps1
. $dir_src/miniconda/activate_opencae.ps1

# Run optimization
if ($keyword -eq "new") {
    $datetime_now = $value
    Set-Location $dir_root/results-$datetime_now # optuna's RDB file will be created here. 
    python $dir_src/optuna/main.py $keyword $datetime_now
} elseif ($keyword -eq "resume") {
    $path_dbfile = $value
    Set-Location (Split-Path $path_dbfile) # optuna's RDB file exists here. 
    python $dir_src/optuna/main.py $keyword $path_dbfile
} else {    
    $error_message = ""
    $error_message += "Wrong keyword!!! `r`n"
    $error_message += "Error at /src/run_optuna.ps1 `r`n"

    Add-Type -AssemblyName System.Windows.Forms;
    [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
}

# Return to the directory where we were
Set-Location $dir_src
